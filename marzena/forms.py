# -*- coding: utf-8 -*-
__author__ = 'karol'


from django import forms

class SubscribeForm(forms.Form):
    email = forms.CharField(label='', max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Email'}), required=True)



class ContactForm(forms.Form):
    name = forms.CharField(label='', max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Imię'}), required=True)
    email = forms.CharField(label='', max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Email'}), required=True)
    subject = forms.CharField(label='', max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Temat'}), required=True)
    message = forms.CharField(label='', max_length=300, widget=forms.Textarea(attrs={'rows': 5, 'cols': 20, 'placeholder': 'Wiadomość'}), required=True)

