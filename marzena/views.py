# -*- coding: UTF-8 -*-
from django.shortcuts import render
from .forms import SubscribeForm, ContactForm
from django.conf import settings
from django.core.mail import send_mail

def home(request):
    form = SubscribeForm(request.POST or None)
    title = "Dołącz do nas i otrzymuj wiadomości o aktualnych promocjach!"
    context = {
        "title": title,
        "form": form
    }

    if form.is_valid():
        title = "Dziękujemy, Twój adres mailowy został zapisany."
        subject = "Maila do subskrypcji."
        to_email = settings.EMAIL_HOST_USER
        from_email = settings.EMAIL_HOST_USER
        new_email = form.cleaned_data["email"].encode('utf-8')
        contact_message =  "Nowa osoba podała swojego maila i prosi o dodanie go do subskrybowanych adresów: " + new_email
        send_mail(subject,
                  contact_message,
                  from_email,
                  [to_email],
                  fail_silently=True)


        context = {
            "title": title,
            "form": form
        }

        return render(request, "home.html", context)


    return render(request, "home.html", context)



def offer(request):
    return render(request, "offer.html")

def promotions(request):
    return render(request, "promotions.html")

def contact(request):
    form = ContactForm(request.POST or None)
    title = "Masz pytania? Napisz do nas!"

    context = {
        "title": title,
        "form": form
    }

    if form.is_valid():
        title = "Wiadomość wysłana. Dziękujemy!"
        name = form.cleaned_data["name"]
        email = form.cleaned_data["email"]
        subject = form.cleaned_data["subject"]
        message = form.cleaned_data["message"]
        from_email = settings.EMAIL_HOST_USER
        to_email = settings.EMAIL_HOST_USER
        contact_message = "Od "+ email +" ("+ name +"): "+ message
        send_mail(subject,
                  contact_message,
                  from_email,
                  [to_email],
                  fail_silently=True)

        context = {
            "form": form,
            "title": title
        }

        return render(request, "contact.html", context)

    return render(request, "contact.html", context)
